#!/usr/bin/python3
import codecs, sys


class Connection:
	def __init__(self, orig: int, dest: int, value: int):
		self.orig = orig
		self.dest = dest
		self.value = value
	
	def __str__(self):
		return str(self.orig) + "--" + str(self.dest) + ":" + str(self.value)


def hasConnection(member, group):
	return member in [x for x in group]


def main(args: [str]):
	myFamily = codecs.open("family.csv", encoding="utf-8").readlines()
	nonFamily = codecs.open("nonfamily.csv", encoding="utf-8").readlines()
	family = myFamily + nonFamily
	graph: [Connection] = []
	chars = set()
	
	i: int = 0
	for f in family:
		line = f.strip().split(";")
		chars.update(line)
	charArray = list(chars)
	safeArray = [x.replace("-", "_").replace(" ", "_") for x in chars]
	boreCount = [0 for x in charArray]
	siredCount = [0 for x in charArray]
	
	matrix = [[0 for i in charArray] for j in charArray]
	for f in family:
		line = f.strip().split(";")
		dam = charArray.index(line[0])
		sire = charArray.index(line[1])
		graph.append(Connection(dam, sire, charArray.index(line[2])))
		boreCount[dam] += 1
		matrix[dam][sire] += 1
		if dam != sire:
			# parthenogenesis: don't make them count as father if they are also the mother
			siredCount[sire] += 1
			matrix[sire][dam] += 1
	
	# print(graph[0])
	# print(charArray[graph[0].orig], charArray[graph[0].dest], charArray[graph[0].value])
	
	output = "graph "
	output += "\"grafo\" {\nnode [width=1.0,height=1.0];\n"
	for i in range(len(charArray)):
		if charArray[i] != "???" and boreCount[i] + siredCount[i] > 0:
			output += safeArray[i] + " [label=\"" + charArray[i] + "\", fontsize=18];\n"
	
	for i in range(len(graph)):
		output += safeArray[graph[i].orig] + " -- " + safeArray[graph[i].dest]
		output += " [label=\"" + charArray[graph[i].value] + "\",weight=1,style=\"setlinewidth(2.0)\""
		output += ",fontsize=16];\n"
	
	output += "}"
	
	if "graph" in args:
		print(output)

	if "chart" in args:
		dist = [[99 for x in charArray] for x in charArray]
		for x in range(len(charArray)):
			dist[x][x] = 0
		for a in range(len(graph)):
			origem = graph[a].orig
			destino = graph[a].dest
			dist[origem][destino] = graph[a].dest
			if dist[origem][destino] > 1:
				dist[origem][destino] = 1
		
		for k in range(len(charArray)):
			for i in range(len(charArray)):
				for j in range(len(charArray)):
					if dist[i][k] + dist[k][j] < dist[i][j]:
						dist[i][j] = dist[i][k] + dist[k][j]
		#print(matrix)
		print(dist[charArray.index("Lea")])
		for x in range(len(dist)):
			for y in range(len(dist)):
				if 3 < dist[x][y] < 90:
					print(dist[x][y], charArray[x], charArray[y])
		print(dist[charArray.index("Lucienne")]) #ESTRANHO
# print(boreCount[charArray.index("Lea")])
# print("Ashany" in charArray)

if __name__ == "__main__":
	main(sys.argv[1:])
